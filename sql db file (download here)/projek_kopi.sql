-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2019 at 12:58 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projek_kopi`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_user`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id_gudang` int(11) NOT NULL,
  `id_kopi_mentah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jual`
--

CREATE TABLE `jual` (
  `id_jual` int(11) NOT NULL,
  `jenis_kopi` varchar(100) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `bobot_stok` int(11) NOT NULL,
  `kualitas` text NOT NULL,
  `ketinggian_tanaman_kopi` int(11) NOT NULL,
  `proses_pasca_panen` text NOT NULL,
  `image_location` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jual`
--

INSERT INTO `jual` (`id_jual`, `jenis_kopi`, `harga`, `bobot_stok`, `kualitas`, `ketinggian_tanaman_kopi`, `proses_pasca_panen`, `image_location`) VALUES
(5, 'Jenis Kopi', 100000, 80, '5', 100, 'proses', 'uploads/kopi_jual_5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jual_keranjang`
--

CREATE TABLE `jual_keranjang` (
  `id_keranjang_jual` int(11) NOT NULL,
  `jenis_kopi` varchar(100) NOT NULL,
  `bobot` bigint(20) NOT NULL,
  `id_jual` int(11) NOT NULL,
  `id_pembeli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jual_pembayaran`
--

CREATE TABLE `jual_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_jual` int(11) NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `bukti_image_upload` text NOT NULL,
  `status` text NOT NULL,
  `jenis_kopi` varchar(100) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jual_pembayaran`
--

INSERT INTO `jual_pembayaran` (`id_pembayaran`, `id_jual`, `id_pembeli`, `bukti_image_upload`, `status`, `jenis_kopi`, `bobot`) VALUES
(4, 5, 1, 'uploads/bukti_pembayaran_2019-12-31_00-30-10.jpg', 'valid', 'Jenis Kopi', 20);

-- --------------------------------------------------------

--
-- Table structure for table `kopi_mentah`
--

CREATE TABLE `kopi_mentah` (
  `id_kopi_mentah` int(11) NOT NULL,
  `jenis_kopi` text NOT NULL,
  `harga` bigint(20) NOT NULL,
  `bobot` bigint(12) NOT NULL,
  `bobot_butuh` int(11) NOT NULL,
  `kualitas` text NOT NULL,
  `jenis_transaksi` varchar(100) NOT NULL,
  `image_location` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lelang`
--

CREATE TABLE `lelang` (
  `id_lelang` int(11) NOT NULL,
  `jenis_kopi` varchar(100) NOT NULL,
  `bobot_lelang` int(11) NOT NULL,
  `kualitas` int(11) NOT NULL,
  `harga_bid_awal` bigint(20) NOT NULL,
  `harga_beli_sekarang` bigint(20) NOT NULL,
  `kelipatan_bid` int(11) NOT NULL,
  `proses_pasca_panen` text NOT NULL,
  `ketinggian_lahan` int(11) NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_berakhir` datetime NOT NULL,
  `image_location` text NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lelang`
--

INSERT INTO `lelang` (`id_lelang`, `jenis_kopi`, `bobot_lelang`, `kualitas`, `harga_bid_awal`, `harga_beli_sekarang`, `kelipatan_bid`, `proses_pasca_panen`, `ketinggian_lahan`, `waktu_mulai`, `waktu_berakhir`, `image_location`, `status`) VALUES
(8, 'Kopi A', 200, 5, 100000, 200000, 6000, 'proses', 0, '2019-12-26 00:00:00', '2019-12-29 07:42:00', 'uploads/kopi_lelang_8.jpg', ''),
(9, 'Kopi B', 200, 2, 200000, 300000, 5000, 'proses', 0, '2019-12-30 00:00:01', '2019-12-30 07:42:00', 'uploads/kopi_lelang_9.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `lelang_bid`
--

CREATE TABLE `lelang_bid` (
  `id_bid` int(11) NOT NULL,
  `id_lelang` int(11) NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `bid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lelang_bid`
--

INSERT INTO `lelang_bid` (`id_bid`, `id_lelang`, `id_pembeli`, `bid`) VALUES
(2, 8, 1, 106000),
(3, 9, 1, 250000);

-- --------------------------------------------------------

--
-- Table structure for table `lelang_keranjang`
--

CREATE TABLE `lelang_keranjang` (
  `id_keranjang` int(11) NOT NULL,
  `id_lelang` int(11) NOT NULL,
  `id_bid` int(11) NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `biaya_penawaran` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lelang_pembayaran`
--

CREATE TABLE `lelang_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_lelang` int(11) NOT NULL,
  `id_bid` int(11) NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `bukti_image_upload` text NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id_pembeli` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id_pembeli`, `id_user`) VALUES
(1, 0),
(2, 4),
(3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `petani`
--

CREATE TABLE `petani` (
  `id_petani` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petani`
--

INSERT INTO `petani` (`id_petani`, `id_user`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_jual_kopi_admin`
--

CREATE TABLE `transaksi_jual_kopi_admin` (
  `id_transaksi_jual` int(11) NOT NULL,
  `id_kopi_mentah` int(11) NOT NULL,
  `bobot` bigint(20) NOT NULL,
  `id_petani` int(11) NOT NULL,
  `jenis_kopi` text NOT NULL,
  `harga` bigint(20) NOT NULL,
  `kualitas` text NOT NULL,
  `tinggi_pohon_kopi` bigint(20) NOT NULL,
  `image_location` text NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_jual_kopi_petani`
--

CREATE TABLE `transaksi_jual_kopi_petani` (
  `id_transaksi_jual` int(11) NOT NULL,
  `id_kopi_mentah` int(11) NOT NULL,
  `bobot` int(11) NOT NULL,
  `id_petani` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nohp` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `nama`, `nohp`, `alamat`) VALUES
(0, 'pembeli@gmail.com', 'password', 'Pembeli 1', '085218123123', 'Malang'),
(1, 'petani@gmail.com', 'password', 'Petani', '085219081823', 'Jalan Panjaitan\r\n'),
(3, 'admin@gmail.com', 'password', 'Admin', '085219081823', 'Jalan Veteran'),
(4, 'pembeli2@gmail.com', 'password', 'Pembeli 2', '085219081823', 'Alamatku'),
(5, 'pembeli3@gmail.com', 'password', 'Pembeli 3', '085219081823', 'Alamat ku 3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id_gudang`);

--
-- Indexes for table `jual`
--
ALTER TABLE `jual`
  ADD PRIMARY KEY (`id_jual`);

--
-- Indexes for table `jual_keranjang`
--
ALTER TABLE `jual_keranjang`
  ADD PRIMARY KEY (`id_keranjang_jual`);

--
-- Indexes for table `jual_pembayaran`
--
ALTER TABLE `jual_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `kopi_mentah`
--
ALTER TABLE `kopi_mentah`
  ADD PRIMARY KEY (`id_kopi_mentah`);

--
-- Indexes for table `lelang`
--
ALTER TABLE `lelang`
  ADD PRIMARY KEY (`id_lelang`);

--
-- Indexes for table `lelang_bid`
--
ALTER TABLE `lelang_bid`
  ADD PRIMARY KEY (`id_bid`);

--
-- Indexes for table `lelang_keranjang`
--
ALTER TABLE `lelang_keranjang`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `lelang_pembayaran`
--
ALTER TABLE `lelang_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- Indexes for table `petani`
--
ALTER TABLE `petani`
  ADD PRIMARY KEY (`id_petani`);

--
-- Indexes for table `transaksi_jual_kopi_admin`
--
ALTER TABLE `transaksi_jual_kopi_admin`
  ADD PRIMARY KEY (`id_transaksi_jual`);

--
-- Indexes for table `transaksi_jual_kopi_petani`
--
ALTER TABLE `transaksi_jual_kopi_petani`
  ADD PRIMARY KEY (`id_transaksi_jual`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id_gudang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jual`
--
ALTER TABLE `jual`
  MODIFY `id_jual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jual_keranjang`
--
ALTER TABLE `jual_keranjang`
  MODIFY `id_keranjang_jual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jual_pembayaran`
--
ALTER TABLE `jual_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kopi_mentah`
--
ALTER TABLE `kopi_mentah`
  MODIFY `id_kopi_mentah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lelang`
--
ALTER TABLE `lelang`
  MODIFY `id_lelang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `lelang_bid`
--
ALTER TABLE `lelang_bid`
  MODIFY `id_bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lelang_keranjang`
--
ALTER TABLE `lelang_keranjang`
  MODIFY `id_keranjang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `lelang_pembayaran`
--
ALTER TABLE `lelang_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id_pembeli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `petani`
--
ALTER TABLE `petani`
  MODIFY `id_petani` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi_jual_kopi_admin`
--
ALTER TABLE `transaksi_jual_kopi_admin`
  MODIFY `id_transaksi_jual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
