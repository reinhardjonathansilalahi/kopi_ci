<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PembayaranModel extends CI_Model
{

    function insert($data_pass)
    {
        $this->db->trans_start();

        $affected_rows = $this->db->insert("jual_pembayaran", $data_pass);
        $inserted_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $inserted_id;
    }

    function edit($id_pembayaran, $data_pass)
    {
        $this->db->where("id_pembayaran", $id_pembayaran);
        return $this->db->update("jual_pembayaran", $data_pass);
    }

    function hapus($id_pembayaran)
    {
        return $this->db->delete("jual_pembayaran", array('id_pembayaran' => $id_pembayaran));
    }

    function getPembayaranPembeli($id_pembeli)
    {
        $this->db->select("jual.*, jual_pembayaran.*");
        $this->db->join("jual", "jual.id_jual=jual_pembayaran.id_jual");
        $this->db->where("id_pembeli", $id_pembeli);
        $query =  $this->db->get('jual_pembayaran');
        return $query;
    }

    function getPembayaran($id_pembayaran)
    {
        $this->db->where("id_pembayaran", $id_pembayaran);
        $query =  $this->db->get('jual_pembayaran', 1);
        return $query;
    }

    function getAllPembayaran()
    {
        $this->db->select("jual.*, jual_pembayaran.*");
        $this->db->join("jual", "jual.id_jual=jual_pembayaran.id_jual");
        $query = $this->db->get("jual_pembayaran");
        return $query;
    }
}
