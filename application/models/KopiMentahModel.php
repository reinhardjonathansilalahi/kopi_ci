<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KopiMentahModel extends CI_Model{

    function insert($data_pass){
        $this->db->trans_start();

        $affected_rows = $this->db->insert("kopi_mentah", $data_pass);
        $inserted_id = $this->db->insert_id();
        
        $this->db->trans_complete();

        return $inserted_id;
    }

    function hapus($id_kopi_mentah){
        return $this->db->delete("kopi_mentah", array('id_kopi_mentah' => $id_kopi_mentah));
    }

    function edit($id_kopi_mentah, $data_pass){
        $this->db->where("id_kopi_mentah", $id_kopi_mentah);
        return $this->db->update("kopi_mentah", $data_pass);
    }

    function getKopiById($id){
        $this->db->where("id_kopi_mentah", $id);
        $query = $this->db->get("kopi_mentah", 1);
        return $query;
    }

    function getAllKopiMentahAdmin(){
        $this->db->where("jenis_transaksi", "kopi_admin");
        $query = $this->db->get("kopi_mentah");
        return $query;
    }

}
