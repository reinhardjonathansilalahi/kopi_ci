<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JualBeliModel extends CI_Model
{
    function insertKeranjang($data_pass)
    {
        $this->db->trans_start();

        $affected_rows = $this->db->insert("jual_keranjang", $data_pass);
        $inserted_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $inserted_id;
    }

    function editKeranjang($id_keranjang_jual, $data_pass)
    {
        $this->db->where("id_keranjang_jual", $id_keranjang_jual);
        return $this->db->update("jual_keranjang", $data_pass);
    }

    function hapusKeranjang($id_keranjang_jual)
    {
        return $this->db->delete("jual_keranjang", array('id_keranjang_jual' => $id_keranjang_jual));
    }
    
    function getKeranjangByJualId($id_jual){
        $this->db->where("id_jual", $id_jual);
        $query =  $this->db->get('jual_keranjang', 1);
        return $query;
    }

    function getKeranjang($id_pembeli)
    {
        $this->db->select("jual.*, jual_keranjang.*");
        $this->db->join("jual", "jual.id_jual=jual_keranjang.id_jual");
        $this->db->where("jual_keranjang.id_pembeli", $id_pembeli);
        $query =  $this->db->get('jual_keranjang');
        return $query;
    }

    function getPembeli($id_jual)
    {
        $this->db->select("jual.*, pembeli.*, user.*");
        $this->db->join("pembeli", "pembeli.id_pembeli=jual.id_pembeli");
        $this->db->join("user", "user.id_user=pembeli.id_user");
        $this->db->where("jual.id_jual", $id_jual);
        $query =  $this->db->get('jual', 1);
        return $query;
    }

    function insert($data_pass)
    {
        $this->db->trans_start();

        $affected_rows = $this->db->insert("jual", $data_pass);
        $inserted_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $inserted_id;
    }

    function edit($id_jual, $data_pass)
    {
        $this->db->where("id_jual", $id_jual);
        return $this->db->update("jual", $data_pass);
    }

    function hapus($id_jual)
    {
        return $this->db->delete("jual", array('id_jual' => $id_jual));
    }

    function getJual($id_jual)
    {
        $this->db->where("id_jual", $id_jual);
        $query =  $this->db->get('jual', 1);
        return $query;
    }

    function getAllJual()
    {
        $query = $this->db->get("jual");
        return $query;
    }
}
