<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LelangModel extends CI_Model
{
    function insertKeranjang($data_pass)
    {
        $this->db->trans_start();

        $affected_rows = $this->db->insert("lelang_keranjang", $data_pass);
        $inserted_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $inserted_id;
    }

    function getMaxBid($id_lelang){
        $this->db->select_max("bid");
        $this->db->where("id_lelang", $id_lelang);
        $query =  $this->db->get('lelang_bid', 1);
        return $query;
    }

    function getKeranjangByLelangId($id_lelang){
        $this->db->where("id_lelang", $id_lelang);
        $query =  $this->db->get('lelang_keranjang', 1);
        return $query;
    }

    function getKeranjang($id_pembeli)
    {
        $this->db->select("lelang.*, lelang_keranjang.*");
        $this->db->join("lelang", "lelang.id_lelang=lelang_keranjang.id_lelang");
        $this->db->where("lelang_keranjang.id_pembeli", $id_pembeli);
        $query =  $this->db->get('lelang_keranjang');
        return $query;
    }

    function getPelelang($id_lelang)
    {
        $this->db->select("lelang_bid.*, pembeli.*, user.*");
        $this->db->join("pembeli", "pembeli.id_pembeli=lelang_bid.id_pembeli");
        $this->db->join("user", "user.id_user=pembeli.id_user");
        $this->db->where("lelang_bid.id_lelang", $id_lelang);
        $query =  $this->db->get('lelang_bid');
        return $query;
    }

    function insertBidLelang($data_pass)
    {
        $this->db->trans_start();

        $affected_rows = $this->db->insert("lelang_bid", $data_pass);
        $inserted_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $inserted_id;
    }

    function editBidLelangByBidId($id_bid, $data_pass)
    {
        $this->db->where("id_bid", $id_bid);
        return $this->db->update("lelang_bid", $data_pass);
    }

    function editBidLelang($id_lelang, $id_pembeli, $data_pass)
    {
        $this->db->where("id_lelang", $id_lelang);
        $this->db->where("id_pembeli", $id_pembeli);
        return $this->db->update("lelang_bid", $data_pass);
    }

    function getBid($data_pass)
    {
        // $this->db->where("id_bid", $id);
        // $query = $this->db->get("lelang_bid", 1);
        // $data_pass = array('usertype' => $status);
        $query =  $this->db->get_where('lelang_bid', $data_pass, 1, 0);
        return $query;
    }

    function insert($data_pass)
    {
        $this->db->trans_start();

        $affected_rows = $this->db->insert("lelang", $data_pass);
        $inserted_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $inserted_id;
    }

    function edit($id_lelang, $data_pass)
    {
        $this->db->where("id_lelang", $id_lelang);
        return $this->db->update("lelang", $data_pass);
    }

    function hapus($id_lelang)
    {
        return $this->db->delete("lelang", array('id_lelang' => $id_lelang));
    }

    function getLelang($id_lelang)
    {
        $this->db->where("id_lelang", $id_lelang);
        $query =  $this->db->get('lelang', 1);
        return $query;
    }

    function getAllLelang()
    {
        $query = $this->db->get("lelang");
        return $query;
    }
}
