<?php

$url = current_url();
$path = explode("/", $url);

?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="<?= base_url() ?>">GIVEN KOPI</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item <?= (end($path) == "") ? "active" : "" ?>">
					<a class="nav-link" href="<?= base_url() ?>">Home</a>
				</li>
				<?php if (!$this->session->has_userdata("role")) : ?>
					<li class="nav-item <?= (end($path) == "login") ? "active" : "" ?>">
						<a class="nav-link" href="<?= base_url("login") ?>">Login</a>
					</li>
					<li class="nav-item <?= (end($path) == "register") ? "active" : "" ?>">
						<a class="nav-link" href="<?= base_url("register") ?>">Register</a>
					</li>
				<?php endif; ?>
				<?php if ($this->session->role == "pembeli" || $this->session->role == "admin") : ?>
					<li class="nav-item <?= (end($path) == "beli") ? "active" : "" ?>">
						<a class="nav-link" href="<?= base_url("beli") ?>">Beli</a>
					</li>
				<?php endif; ?>
				<?php if ($this->session->role == "penjual" || $this->session->role == "admin") : ?>
					<li class="nav-item <?= (end($path) == "jual") ? "active" : "" ?>">
						<a class="nav-link" href="<?= base_url("jual") ?>">Jual</a>
					</li>
				<?php endif; ?>
				<?php if ($this->session->role == "pembeli" || $this->session->role == "admin") : ?>
					<li class="nav-item <?= (end($path) == "lelang") ? "active" : "" ?>">
						<a class="nav-link" href="<?= base_url("lelang") ?>">Lelang</a>
					</li>
				<?php endif; ?>
				<?php if ($this->session->role == "pembeli") : ?>
					<li class="nav-item <?= (end($path) == "keranjang") ? "active" : "" ?>">
						<a class="nav-link" href="<?= base_url("keranjang") ?>">Keranjang</a>
					</li>
				<?php endif; ?>
				<?php if ($this->session->role == "pembeli" || $this->session->role == "admin") : ?>
					<li class="nav-item <?= (end($path) == "pembayaran") ? "active" : "" ?>">
						<a class="nav-link" href="<?= base_url("pembayaran") ?>">Pembayaran</a>
					</li>
				<?php endif; ?>

				<li class="nav-item">
					<div class="dropdown" style="margin-top: 0px">
						<button style="margin: 0px;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
							Pengaturan
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="<?= base_url("logout") ?>">Logout</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>
