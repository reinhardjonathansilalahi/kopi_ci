﻿<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>GIVEN KOPI - JUAL & BELI KOPI</title>

  <!-- Bootstrap core CSS -->
  <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="assets/css/shop-homepage.css" rel="stylesheet">

  <!-- Custom styles -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/sweetalert2/sweetalert2.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/toastr/toastr.min.css">

</head>

<body>

  <div id="main-wrapper">

    <!-- Navigation Start -->
    <?php
    $this->load->view("components/member_header")
    ?>
    <!-- Navigation End -->

    <!-- Page Content -->
    <div class="container" style="padding-top: 80px;">

      <div class="row">

        <!-- /.col-lg-3 -->

        <div class="col-lg-12">


          <div class="row" style="width: 100%; margin: 0 auto; padding: 0px 0px 30px 0px;">
            <div class="btn btn-primary" style="width: 100%;">
              Daftar kopi yang dicari Admin
            </div>
          </div>

          <div class="row">

            <?php foreach ($rows as $row) : ?>

              <div class="col-lg-4 col-md-4 mb-4">
                <div class="card h-100">
                  <!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
                  <div class="card-body">
                    <img src="<?= $row->image_location ?>" style="width: 100%; margin: 0 auto;" />
                    <h4 class="card-title" style="margin-top: 20px">
                      <a href="#"><?= $row->jenis_kopi ?></a>
                    </h4>
                    <p class="card-text">Bobot butuh : <?= $row->bobot_butuh ?></p>
                    <p class="card-text">Bobot sekarang : <?= $row->bobot ?></p>
                  </div>
                  <div class="card-footer">
                    <button class="btn btn-primary" onclick="document.getElementById('modalform-<?= $row->id_kopi_mentah ?>').style.display='block'" style="width:auto;">Pilih</button>
                  </div>
                </div>
              </div>

              <!-- modal form -->
              <div id="modalform-<?= $row->id_kopi_mentah ?>" class="modal">
                <form id="tambahJualKopiMentah" method="post">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Jual Kopi</h5>
                        <button type="button" class="close" onclick="document.getElementById('modalform-<?= $row->id_kopi_mentah ?>').style.display='none'" data-dismiss="modalform" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">

                        <div class="form-group">
                          <label for="gambarKopi">Gambar Kopi</label>
                          <input type="file" name="gambar_kopi" class="form-control-file" id="gambarKopi">
                        </div>

                        <div class="form-group">
                          <label for="jenis_kopi">Jenis Kopi</label>
                          <input type="text" name="jenis_kopi" class="form-control" id="jenis_kopi" placeholder="Jenis Kopi">
                        </div>

                        <div class="form-group">
                          <label for="harga">Harga</label>
                          <input type="number" name="harga" class="form-control" id="harga" placeholder="Harga (per kg)">
                        </div>

                        <div class="form-group">
                          <label for="bobot">Bobot</label>
                          <input type="number" name="bobot" class="form-control" id="bobot" placeholder="Masukkan bobot">
                        </div>

                        <div class="form-group">
                          <label for="kualitas">Kualitas</label>
                          <!-- <textarea class="form-control" id="kualitas" name="kualitas" placeholder="Kualitas Kopi" rows="3"></textarea> -->
                          <input type="number" name="kualitas" class="form-control" id="kualitas" placeholder="Masukkan kualitas, 1 sampai 5...">
                        </div>

                        <div class="form-group">
                          <label for="tinggi_pohon_kopi">Tinggi rata-rata pohon</label>
                          <input type="number" name="tinggi_pohon_kopi" class="form-control" id="tinggi_pohon_kopi" placeholder="Tingi rata-rata pohon kopi (cm)">
                        </div>

                        <input type="hidden" name="id_kopi_mentah" value="<?= $row->id_kopi_mentah ?>" />
                        <input type="hidden" name="id_petani" value="<?= $this->session->id_petani ?>" />

                      </div>
                      <div class="modal-footer">
                        <button type="button" onclick="document.getElementById('modalform-tambahJual').style.display='none'" class="cancelbtn">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

            <?php endforeach; ?>

          </div>
          <!-- /.row -->




          <?php if (count($jual_kopi_belum_valid_rows) > 0) : ?>
            <div class="row" style="width: 100%; margin: 0 auto; padding: 0px 0px 30px 0px;">
              <div class="btn btn-secondary" style="width: 100%;">
                Daftar jual kopi saya (Belum divalidasi)
              </div>
            </div>
          <?php endif; ?>

          <div class="row">

            <?php foreach ($jual_kopi_belum_valid_rows as $row) : ?>
              <div class="col-lg-4 col-md-4 mb-4">
                <div class="card h-100">
                  <!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
                  <div class="card-body">
                    <img src="<?= $row->image_location ?>" style="width: 100%; margin: 0 auto;" />
                    <h4 class="card-title" style="margin-top: 20px">
                      <a href="#"><?= $row->jenis_kopi ?></a>
                    </h4>
                    <p class="card-text">Harga penawaran (per kg) : <?= $row->harga ?></p>
                    <p class="card-text">Bobot yg dijual: <?= $row->bobot ?></p>
                    <p class="card-text">Kualitas : <?= $row->kualitas ?></p>
                  </div>
                  <div class="card-footer">
                    <button class="btn btn-primary" onclick="document.getElementById('modalform-<?= $row->id_kopi_mentah ?>').style.display='block'" style="width:auto;">Pilih</button>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>

          </div>





          <?php if (count($jual_kopi_sudah_valid_rows) > 0) : ?>
            <div class="row" style="width: 100%; margin: 0 auto; padding: 0px 0px 30px 0px;">
              <div class="btn btn-success" style="width: 100%;">
                Daftar jual kopi saya (Struk)
              </div>
            </div>
          <?php endif; ?>


          <div class="row">

            <?php foreach ($jual_kopi_sudah_valid_rows as $row) : ?>
              <div class="col-lg-4 col-md-4 mb-4">
                <div class="card h-100">
                  <!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
                  <div class="card-body">
                    <img src="<?= $row->image_location ?>" style="width: 100%; margin: 0 auto;" />
                    <h4 class="card-title" style="margin-top: 20px">
                      <a href="#"><?= $row->jenis_kopi ?></a>
                    </h4>
                    <p class="card-text">Nama Petani : <?= $row->nama ?></p>
                    <p class="card-text">Jenis Kopi : <?= $row->jenis_kopi ?></p>
                    <p class="card-text">Kualitas : <?= $row->kualitas ?></p>
                    <p class="card-text">Tinggi Rata-Rata Pohon : <?= $row->tinggi_pohon_kopi ?></p>
                    <p class="card-text">Bobot (kg) : <?= $row->bobot ?></p>
                    <p class="card-text">Harga penawaran (per kg) : Rp<?= $row->harga ?></p>
                    <p class="card-text">Harga Total (kg) : <?= $row->harga * $row->bobot ?></p>
                  </div>
                  <div class="card-footer">
                    <button class="btn btn-primary" onclick="document.getElementById('modalform-<?= $row->id_kopi_mentah ?>').style.display='block'" style="width:auto;">Pilih</button>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>

          </div>


        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->


    <?php
    $this->load->view("components/footer")
    ?>

  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="assets/jquery/jquery.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

  <script>
    const Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 5000
    });

    // tambahJualKopiMentah

    $('form[id=tambahJualKopiMentah').submit(function(e) {
      e.preventDefault();

      $('.form-group').removeClass('has-error'); // remove the error class
      $('.help-block').remove(); // remove the error text
      $('.alert-success').remove();

      // var formData = {};

      // var datas = $(this).serializeArray();
      // datas.map(function(item, index, array) {
      //   formData[item.name] = item.value;
      // });

      // alert("Submitted");
      // alert(JSON.stringify(formData));

      var formUrl = "<?= base_url("jualKopiMentahAdmin") ?>";

      // process the form
      $.ajax({
          type: 'POST',
          url: formUrl,
          data: new FormData(this), //penggunaan FormData
          dataType: 'json', // what type of data do we expect back from the serverss
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          error: function(data) {
            alert("AJAX ERROR")
            alert(JSON.stringify(data));
          }
        })

        // using the done promise callback
        .done(function(data) {

          // here we will handle errors and validation messages
          if (!data.success) {

            Toast.fire({
              type: 'error',
              title: data.message
            });

          } else {

            // ALL GOOD! just show the success message!
            // $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
            Toast.fire({
              type: 'success',
              title: data.message
            });

            setTimeout(function() {
              window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
            }, 1000); //will call the function after 2 secs.

          }
        });
    });
  </script>

</body>

</html>
