﻿<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="assets/plugins/toastr/toastr.min.css">
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="#">GIVEN KOPI</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<button onclick="document.getElementById('formlogin').style.display='block'" style="width:auto;">Login</button>
				<li class="nav-item">
					<button onclick="document.getElementById('formRegister').style.display='block'" style="width:auto;">Daftar</button>
			</ul>
		</div>
	</div>
</nav>

<!-- carousel -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="margin-top: 30px">
	<ol class="carousel-indicators">
		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner" role="listbox">
		<div class="carousel-item active">
			<img class="d-block img-fluid" src="assets/img/kopi.jpg" alt="First slide">
			<div class="carousel-caption d-none d-md-block">
				<h5>First slide label</h5>
				<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block img-fluid" src="assets/img/kopi.jpg" alt="Second slide">
			<div class="carousel-caption d-none d-md-block">
				<h5>First slide label</h5>
				<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
			</div>
		</div>
		<div class="carousel-item">
			<img class="d-block img-fluid" src="assets/img/kopi.jpg" alt="Third slide">
			<div class="carousel-caption d-none d-md-block">
				<h5>First slide label</h5>
				<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
			</div>
		</div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>

<!-- modal login -->
<div id="formlogin" class="modal" style="margin-top: 80px">

	<!-- Modal Content -->
	<form id="loginForm" class="modal-content animate" method="post">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md">
						<label for="email"><b>Email</b></label>
						<input type="text" id="email" placeholder="Enter email" name="email" required>
					</div>
					<div class="col-md">
						<label for="password"><b>Password</b></label>
						<input type="password" id="password" placeholder="Enter Password" name="password" required>
					</div>
				</div>
				<label for="role"><b>Login sebagai</b></label>
				<select id="role" name="role" class="form-control form-control">
					<option value="admin">Admin</option>
					<option value="penjual">Penjual</option>
					<option value="pembeli">Pembeli</option>
				</select>
			</div>

			<button type="submit">Login</button>
		</div>

		<div class="container" style="background-color:#f1f1f1">
			<button type="button" onclick="document.getElementById('formlogin').style.display='none'" class="cancelbtn">Cancel</button>
			<span class="psw">Forgot <a href="#">password?</a></span>
		</div>
	</form>
</div>

<div id="formRegister" class="modal">
	<!-- Modal Content -->
	<form id="registerForm" class="modal-content animate" method="post">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md">
						<label for="email"><b>Email</b></label>
						<input type="text" placeholder="Enter email" name="email" required>
					</div>
					<div class="col-md">
						<label for="password"><b>Password</b></label>
						<input type="password" placeholder="Enter Password" name="password" required>
					</div>
				</div>

				<div class="row">
					<div class="col-md">
						<label for="nama"><b>Nama</b></label>
						<input type="text" placeholder="Enter nama" name="nama" required>
					</div>

					<div class="col-md">
						<label for="nohp"><b>No HP</b></label>
						<input type="text" placeholder="Enter nohp" name="nohp" required>
					</div>

					<div class="col-md">
						<label for="alamat"><b>Alamat</b></label>
						<input type="text" placeholder="Enter alamat" name="alamat" required>
					</div>
				</div>

				<label for="role"><b>Daftar sebagai</b></label>
				<select name="role" class="form-control form-control">
					<option value="admin">Admin</option>
					<option value="penjual">Penjual</option>
					<option value="pembeli">Pembeli</option>
				</select>
			</div>

			<button type="submit">Daftar</button>
		</div>

		<div class="container" style="background-color:#f1f1f1">
			<button type="button" onclick="document.getElementById('formRegister').style.display='none'" class="cancelbtn">Cancel</button>
		</div>
	</form>
</div>

<?php
$this->load->view("components/footer")
?>

<!-- Bootstrap core JavaScript -->
<script src="assets/jquery/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<script>
	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});

	$('form[id=loginForm').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formData = {};

		var datas = $(this).serializeArray();
		datas.map(function (item, index, array) {
			formData[item.name] = item.value;
		});

		// alert("Submitted");
		// alert(JSON.stringify(formData));

		var formUrl = "";
		switch (formData.role) {
			case "admin":
				formUrl = "<?= base_url("AdminController/login") ?>";
				break;
			case "penjual":
				formUrl = "<?= base_url("PenjualController/login") ?>";
				break;
			case "pembeli":
				formUrl = "<?= base_url("PembeliController/login") ?>";
				break;
		}

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: formData, // data object
			dataType: 'json', // what type of data do we expect back from the serverss
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						document.getElementById('formlogin').style.display = 'none'

						setTimeout(function () {
							window.location.href = "<?= base_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});
</script>

<script>
	$('form[id=registerForm').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formData = {};

		var datas = $(this).serializeArray();
		datas.map(function (item, index, array) {
			formData[item.name] = item.value;
		});

		// alert("Submitted");
		// alert(JSON.stringify(formData));

		var formUrl = "";
		switch (formData.role) {
			case "admin":
				formUrl = "<?= base_url("AdminController/register") ?>";
				break;
			case "penjual":
				formUrl = "<?= base_url("PenjualController/register") ?>";
				break;
			case "pembeli":
				formUrl = "<?= base_url("PembeliController/register") ?>";
				break;
		}

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: formData, // data object
			dataType: 'json', // what type of data do we expect back from the serverss
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						document.getElementById('formRegister').style.display = 'none'

					}
				});
	});
</script>

</body>

</html>
