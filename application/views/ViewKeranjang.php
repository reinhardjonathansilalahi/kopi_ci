﻿<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- Custom styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/sweetalert2/sweetalert2.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/toastr/toastr.min.css">

</head>

<body>

<!-- Navigation Start -->
<?php
$this->load->view("components/member_header")
?>
<!-- Navigation End -->


<!-- Page Content -->
<div class="container mt-4" style="min-height: 500px">
	<div class="row">
		<div class="col-lg-12">
			<div class="row">

				<div class="row" style="width: 100%; margin: 0 auto; padding: 0px 35px 30px 35px;">
					<button class="btn btn-secondary" style="width: 100%;"
							onclick="document.getElementById('tambahKopiMentahModal').style.display='block'">
						Keranjang Beli
					</button>
				</div>

				<?php foreach ($beli_rows as $row) : ?>
					<div class="col-lg-12 col-md-12 mb-4">
						<div class="card h-100">
							<div class="card-body">
								<div class="row">
									<div class="col-lg-4 col-md-4" align="center">
										<a href="uploads/kopi_mentah_1.jpg" target="_blank">
											<img style="max-height: 150px; width: auto;" class="card-img-top"
												 src="<?= base_url($row->image_location) ?>" alt="">
										</a>
									</div>
									<div class="col-lg-8 col-md-8">
										<h4 class="card-title">
											<a href="#"><?= $row->jenis_kopi ?></a>
										</h4>
										<p class="card-text">Bobot : <?= $row->bobot ?></p>
									</div>
								</div>
							</div>

							<div align="right" style="margin-bottom: 30px; margin-right: 30px;">
								<button id="bayarBeli" onclick="document.getElementById('bayarBeliModal<?= $row->id_jual ?>').style.display='block'"
										type="button" class="btn btn-success">Checkout
								</button>
								<button id="batalBeli" onclick="document.getElementById('hapusBeliModal<?= $row->id_jual ?>').style.display='block'"
										type="button" class="btn btn-danger">Batal
								</button>
							</div>
						</div>
					</div>

					<!-- modal form -->
					<div id="bayarBeliModal<?= $row->id_jual ?>" class="modal">
						<form id="bayarBeli" method="post">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Checkout belanjaan</h5>
										<button type="button" class="close"
												onclick="document.getElementById('bayarBeliModal<?= $row->id_jual ?>').style.display='none'"
												data-dismiss="editModalform" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<p>Apakah yakin ingin melakukan checkout?</p>
										<input type="hidden" name="jenis_kopi" value="<?= $row->jenis_kopi ?>"/>
										<input type="hidden" name="bobot" value="<?= $row->bobot ?>"/>
										<input type="hidden" name="id_jual" value="<?= $row->id_jual ?>"/>
										<input type="hidden" name="id_pembeli" value="<?= $id_pembeli ?>"/>
										<input type="hidden" name="id_keranjang_jual" value="<?= $row->id_keranjang_jual ?>"/>
									</div>
									<div class="modal-footer">
										<!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
										<button type="submit"
												onclick="document.getElementById('bayarBeliModal<?= $row->id_jual ?>').style.display='none'"
												class="btn btn-primary">Ya
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>

					<!-- modal form -->
					<div id="hapusBeliModal<?= $row->id_jual ?>" class="modal">
						<form id="hapusBeli" method="post">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Bayar belanjaan</h5>
										<button type="button" class="close"
												onclick="document.getElementById('hapusBeliModal<?= $row->id_jual ?>').style.display='none'"
												data-dismiss="editModalform" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<p>Apakah yakin ingin membatalkan belanja?</p>
										<input type="hidden" name="id_jual" value="<?= $row->id_jual ?>"/>
										<input type="hidden" name="bobot" value="<?= $row->bobot ?>"/>
										<input type="hidden" name="id_keranjang_jual" value="<?= $row->id_keranjang_jual ?>"/>
									</div>
									<div class="modal-footer">
										<!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
										<button type="submit"
												onclick="document.getElementById('hapusBeliModal<?= $row->id_jual ?>').style.display='none'"
												class="btn btn-primary">Ya
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				<?php endforeach; ?>

				<div class="row" style="width: 100%; margin: 0 auto; padding: 0px 50px 30px 20px;">
					<button class="btn btn-secondary" style="width: 100%;"
							onclick="document.getElementById('tambahKopiMentahModal').style.display='block'">
						Keranjang Lelang
					</button>
				</div>

				<div class="row">
					<?php foreach ($lelang_rows as $row) : ?>
						<div class="col-lg-12 col-md-12 mb-4">
							<div class="card h-100">
								<div class="card-body">
									<div class="row">
										<div class="col-lg-4 col-md-4" align="center">
											<a href="uploads/kopi_mentah_1.jpg" target="_blank">
												<img style="max-height: 150px; width: auto;" class="card-img-top"
													 src="<?= base_url($row->image_location) ?>" alt="">
											</a>
										</div>
										<div class="col-lg-8 col-md-8">
											<h4 class="card-title">
												<a href="#"><?= $row->jenis_kopi ?></a>
											</h4>
											<p class="card-text">Biaya penawaran : <?= $row->biaya_penawaran ?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<!-- /.row -->

			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
</div>


<?php
$this->load->view("components/footer")
?>

<!-- Bootstrap core JavaScript -->
<!-- Jquery JS-->
<script src="assets/admin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<script>
	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});
</script>

<script>
	$(document).ready(function () {

		$('form[id=bayarBeli').submit(function (e) {
			e.preventDefault();

			$('.form-group').removeClass('has-error'); // remove the error class
			$('.help-block').remove(); // remove the error text
			$('.alert-success').remove();

			var formUrl = "<?= base_url("bayarBeli") ?>";

			// process the form
			$.ajax({
				type: 'POST',
				url: formUrl,
				data: new FormData(this), //penggunaan FormData
				dataType: 'json', // what type of data do we expect back from the serverss
				processData: false,
				contentType: false,
				cache: false,
				async: false,
				error: function (data) {
					alert("AJAX ERROR")
					alert(JSON.stringify(data));
				}
			})

					// using the done promise callback
					.done(function (data) {

						// here we will handle errors and validation messages
						if (!data.success) {

							Toast.fire({
								type: 'error',
								title: data.message
							});

						} else {

							// ALL GOOD! just show the success message!
							// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
							Toast.fire({
								type: 'success',
								title: data.message
							});

							setTimeout(function () {
								window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
							}, 1000); //will call the function after 2 secs.

						}
					});
		});


		$('form[id=hapusBeli').submit(function (e) {
			e.preventDefault();

			$('.form-group').removeClass('has-error'); // remove the error class
			$('.help-block').remove(); // remove the error text
			$('.alert-success').remove();

			var formUrl = "<?= base_url("hapusKeranjang") ?>";

			// process the form
			$.ajax({
				type: 'POST',
				url: formUrl,
				data: new FormData(this), //penggunaan FormData
				dataType: 'json', // what type of data do we expect back from the serverss
				processData: false,
				contentType: false,
				cache: false,
				async: false,
				error: function (data) {
					alert("AJAX ERROR")
					alert(JSON.stringify(data));
				}
			})

					// using the done promise callback
					.done(function (data) {

						// here we will handle errors and validation messages
						if (!data.success) {

							Toast.fire({
								type: 'error',
								title: data.message
							});

						} else {

							// ALL GOOD! just show the success message!
							// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
							Toast.fire({
								type: 'success',
								title: data.message
							});

							setTimeout(function () {
								window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
							}, 1000); //will call the function after 2 secs.

						}
					});
		});

	});
</script>

</body>

</html>
