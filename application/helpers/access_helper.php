<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (! function_exists('admin_check')) {
    function admin_check($argVal = '')
    {
        $ci = get_instance();

        if (($ci->session->userdata('logged_in') != true) || ($ci->session->userdata('email') != "admin")) {
            return false;
        } else {
            return true;
        }
    }
}

if (! function_exists('admin_access_check')) {
    function admin_access_check($argVal = '')
    {
        $ci = get_instance();

        if (($ci->session->userdata('logged_in') != true) || ($ci->session->userdata('email') != "admin")) {
            header('HTTP/1.1 403 Your access is denied');
            header('Status: 403 Your access is denied');
            header('Retry-After: 300');
            echo "Your access is denied, this is for admin only";

            if (!empty($_SERVER['HTTP_REFERER'])) {
                header("refresh:2; url=".$_SERVER['HTTP_REFERER']);
            } else {
                header("refresh:2; url=".base_url());
            }
            return false;
        } else {
            return true;
        }
    }
}


if (! function_exists('user_access_check')) {
    function user_access_check($argVal = '')
    {
        $ci = get_instance();

        if (($ci->session->userdata('logged_in') != true)) {
            header('HTTP/1.1 403 Your access is denied');
            header('Status: 403 Your access is denied');
            header('Retry-After: 300');
            echo "Your access is denied, this is for logged-in user only";

            if (!empty($_SERVER['HTTP_REFERER'])) {
                header("refresh:2; url=".$_SERVER['HTTP_REFERER']);
            } else {
                header("refresh:2; url=".base_url());
            }
            return false;
        } else {
            return true;
        }
    }
}
