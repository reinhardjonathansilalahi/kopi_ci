<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait KopiMentahController
{

	public function tambahKopiMentah()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		$error_string = checkFile($_FILES['gambar_kopi']);
		if (!empty($error_string)) {
			$data['message'] = $error_string;
			$data['success'] = false;
			echo json_encode($data);
			return;
		}

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			unset($data_pass["gambar_kopi"]);
			$data_pass["jenis_transaksi"] = "kopi_admin";
			$insertedId = $this->kopimentah_model->insert($data_pass);
			$isSuccess = $insertedId;

			if ($isSuccess) {
				# Upload gambar
				# -------------------------------------------------------------------
				$config['upload_path'] = "./uploads/";
				$config['file_name'] = "kopi_mentah_" . $insertedId;
				$config['allowed_types'] = 'gif|jpg|png';
				// $config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload("gambar_kopi")) {
					$data['message'] = $this->upload->display_errors();
					$data['errors'] = $errors;
					$data['success'] = false;
					echo json_encode($data);
					return;
				}
			}

			if ($isSuccess) {
				$data_image = array(
					"image_location" => "uploads/{$config['file_name']}" . $this->upload->data('file_ext'),
				);
				$isSuccess = $this->kopimentah_model->edit($insertedId, $data_image);
			}
			# -------------------------------------------------------------------


			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Tambah kopi mentah berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Tambah kopi mentah gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function editKopiMentahAdmin()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$id_kopi_mentah = $data_pass["id_kopi_mentah"];

			if (isset($_FILES['gambar_kopi']) && is_uploaded_file($_FILES['gambar_kopi']['tmp_name'])) {
				# Upload &replace gambar
				# -------------------------------------------------------------------
				$config['upload_path'] = "./uploads/";
				$config['file_name'] = "kopi_mentah_" . $id_kopi_mentah;
				$config['allowed_types'] = 'gif|jpg|png';
				$config["overwrite"] = TRUE;
				// $config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);

				$filename_with_ext = "./uploads/{$config['file_name']}" . $this->upload->data('file_ext');

				if (file_exists($filename_with_ext)) {
					unlink($filename_with_ext);
				}

				if (!$this->upload->do_upload("gambar_kopi")) {
					$data['message'] = $this->upload->display_errors();
					$data['errors']  = $errors;
					$data['success'] = false;
					echo json_encode($data);
					return;
				} else {
					$data_pass["image_location"] = "uploads/{$config['file_name']}" . $this->upload->data('file_ext');
				}

				# -------------------------------------------------------------------
			}


			$data_pass["jenis_transaksi"] = "kopi_admin";
			unset($data_pass["id_kopi_mentah"]);

			$isSuccess = $this->kopimentah_model->edit($id_kopi_mentah, $data_pass);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Edit kopi mentah berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Edit kopi mentah gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function hapusKopiMentahAdmin()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$id_kopi_mentah = $data_pass["id_kopi_mentah"];
			$isSuccess = $this->kopimentah_model->hapus($id_kopi_mentah);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Hapus kopi mentah berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Hapus kopi mentah gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}


	// public function jualKopiMentahPetani()
	// {
	// 	$error         = "";      // array to hold validation errors
	// 	$data           = array();      // array to pass back data
	// 	$data_pass 		= array();

	// 	foreach ($_POST as $key => $value) {
	// 		$data_pass[$key] = $value;
	// 	}

	// 	$id_petani = $data_pass["id_petani"];
	// 	$bobot = $data_pass["bobot"];
	// 	$harga = $data_pass["harga"];

	// 	unset($data_pass["id_petani"]);

	// 	if ($harga > 15000) {
	// 		$error = "Maaf harga yg anda tawarkan tidak sesuai. Maksimal harga perkilo 15.000";
	// 	} else if ($bobot < 200) {
	// 		$error = "Minimal bobot 200 kg";
	// 	}

	// 	// if there are any errors in our errors array, return a success boolean of false
	// 	if (!empty($error)) {

	// 		// if there are items in our errors array, return those errors
	// 		$data['success'] = false;
	// 		$data['errors']  = $error;
	// 		$data['message'] = $error;
	// 	} else {

	// 		$data_pass["jenis_transaksi"] = "kopi_petani";
	// 		$id_kopi_mentah = $this->kopimentah_model->insert($data_pass);


	// 		if (isset($_FILES['gambar_kopi']) && is_uploaded_file($_FILES['gambar_kopi']['tmp_name'])) {
	// 			# Upload gambar
	// 			# -------------------------------------------------------------------
	// 			$config['upload_path'] = "./uploads/";
	// 			$config['file_name'] = "kopi_mentah_" . $id_kopi_mentah;
	// 			$config['allowed_types'] = 'gif|jpg|png';
	// 			// $config['encrypt_name'] = TRUE;

	// 			$this->load->library('upload', $config);


	// 			if (!$this->upload->do_upload("gambar_kopi")) {
	// 				$data['message'] = $this->upload->display_errors();
	// 				$data['success'] = false;
	// 				echo json_encode($data);
	// 				return;
	// 			}

	// 			if ($id_kopi_mentah) {
	// 				$data_image = array(
	// 					"image_location" => "uploads/{$config['file_name']}" . $this->upload->data('file_ext'),
	// 				);

	// 				$this->kopimentah_model->edit($id_kopi_mentah, $data_image);
	// 			}
	// 			# -------------------------------------------------------------------
	// 		}

	// 		$data_jual_kopi = array(
	// 			"id_kopi_mentah" => $id_kopi_mentah,
	// 			"id_petani" => $id_petani,
	// 			"bobot" => $bobot,
	// 		);

	// 		$isSuccess = $this->jualkopi_model->insertJualKopiPetani($data_jual_kopi);

	// 		// DO ALL YOUR FORM PROCESSING HERE
	// 		// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

	// 		// show a message of success and provide a true success variable
	// 		if ($isSuccess) {
	// 			$data['success'] = true;
	// 			$data['message'] = 'Tambah jual kopi mentah berhasil!';
	// 		} else {
	// 			$data['success'] = false;
	// 			$data['errors']  = $error;
	// 			$data['message'] = 'Tambah jual kopi mentah gagal!';

	// 			if (!empty($error)) {
	// 				$data["message"] = $error;
	// 			}
	// 		}
	// 	}

	// 	// return all our data to an AJAX call
	// 	echo json_encode($data);
	// }
}
