<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait PembayaranController
{

	public function bayarBeli()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$id_keranjang_jual = $data_pass["id_keranjang_jual"];
			unset($data_pass["id_keranjang_jual"]);

			$data_pass["status"] = "konfirmasi";
			$isSuccess = $this->pembayaran_model->insert($data_pass);

			$isSuccess = $this->jualbeli_model->hapusKeranjang($id_keranjang_jual);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Checkout belanjaan berhasil! Silakan konfirmasi pembayaran.';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Checkout belanjaan gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}


	public function konfirmasiPembayaran()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		$error_string = checkFile($_FILES['bukti_pembayaran']);
		if (!empty($error_string)) {
			$data['message'] = $error_string;
			$data['success'] = false;
			echo json_encode($data);
			return;
		}

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			unset($data_pass["bukti_pembayaran"]);
			$id_pembayaran = $data_pass["id_pembayaran"];
			$isSuccess = 0;

			$date = new DateTime();
			$current_datetime = $date->format("Y-m-d_H-i-s");

			# Upload gambar
			# -------------------------------------------------------------------
			$config['upload_path'] = "./uploads/";
			$config['file_name'] = "bukti_pembayaran_" . $current_datetime;
			$config['allowed_types'] = 'gif|jpg|png';
			// $config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload("bukti_pembayaran")) {
				$data['message'] = $this->upload->display_errors();
				$data['errors']  = $errors;
				$data['success'] = false;
				echo json_encode($data);
				return;
			}

			$data_pass = array(
				"status" => "verifikasi",
				"bukti_image_upload" => "uploads/{$config['file_name']}" . $this->upload->data('file_ext'),
			);

			$isSuccess = $this->pembayaran_model->edit($id_pembayaran, $data_pass);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Konfirmasi pembayaran berhasil dikirim.';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Konfirmasi pembayaran gagal dikirim!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}
}
