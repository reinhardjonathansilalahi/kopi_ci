<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "controllers/admin/KopiMentahController.php";
require_once APPPATH . "controllers/admin/LelangController.php";
require_once APPPATH . "controllers/admin/KeranjangController.php";
require_once APPPATH . "controllers/admin/JualController.php";
require_once APPPATH . "controllers/admin/PembayaranController.php";

class AdminController extends CI_Controller
{
	USE KopiMentahController;
	USE LelangController;
	USE KeranjangController;
	USE JualController;
	USE PembayaranController;

	function __construct()
	{
		parent::__construct();
		$this->load->model('MemberModel', "member_model");
		$this->load->model("KopiMentahModel", "kopimentah_model");
		$this->load->model("JualKopiModel", "jualkopi_model");
		$this->load->model("LelangModel", "lelang_model");
		$this->load->model("JualBeliModel", "jualbeli_model");
		$this->load->model("PembayaranModel", "pembayaran_model");
	}

	public function index()
	{
		$this->load->view('ViewHome');
	}

	public function tambah_beli()
	{
		$this->load->view('AdminViewBeli');
	}

	public function login()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		$data_pass["table_name"] = "admin";

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$isSuccess = false;

			// if there are no errors process our form, then return a message
			if ($this->member_model->getMember($data_pass)->num_rows() > 0) {
				$isSuccess = true;
				$this->session->set_userdata("role", $data_pass["role"]);
			}

			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Login success!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Login failed!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function register()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		$table_name = "admin";

		// Unset the non column key value data_pass for inserting new row item for database table
		unset($data_pass["role"]);

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			// the value inside isSucces is the insertedRow Count
			$isSuccess = $this->member_model->insert($table_name, $data_pass);

			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Register success!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Register failed!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function ubahStatus(){
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		// Unset the non column key value data_pass for inserting new row item for database table
		unset($data_pass["role"]);

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$id_transaksi_jual = $data_pass["id_transaksi_jual"];

			// the value inside isSucces is the insertedRow Count
			unset($data_pass["id_transaksi_jual"]);
			$isSuccess = $this->jualkopi_model->edit($id_transaksi_jual, $data_pass);

			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Ubah status penjualan berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = $errors;
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
		
	}

}
