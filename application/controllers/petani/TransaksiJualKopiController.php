<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait TransaksiJualKopiController
{
	public function jualKopiMentahAdmin()
	{
		$error         		= "";      // array to hold validation errors
		$data           	= array();      // array to pass back data
		$data_jual_kopi 	= array();

		$error_string = checkFile($_FILES['gambar_kopi']);
		if (!empty($error_string)) {
			$data['message'] = $error_string;
			$data['success'] = false;
			echo json_encode($data);
			return;
		}

		foreach ($_POST as $key => $value) {
			$data_jual_kopi[$key] = $value;

			if (empty($value)) {
				$error = "Mohon lengkapi data {$key} pada form";
				break;
			}
		}

		if (!empty($error)) {
			$data['success'] = false;
			$data['errors']  = $error;
			$data['message'] = $error;
		} else {

			$id_kopi_mentah = $data_jual_kopi["id_kopi_mentah"];
			$id_petani = $data_jual_kopi["id_petani"];
			$bobot_jual = $data_jual_kopi["bobot"];
			$jenis_kopi = $data_jual_kopi["jenis_kopi"];
			$harga = $data_jual_kopi["harga"];
			$kualitas = $data_jual_kopi["kualitas"];

			$isSuccess = false;
			$isSuccess2 = false;

			$kopi_mentah = $this->kopimentah_model->getKopiById($id_kopi_mentah)->row();
			$bobot_butuh = $kopi_mentah->bobot_butuh;
			$bobot_sekarang = $kopi_mentah->bobot;
			$bobot_bertambah = $bobot_sekarang + $bobot_jual;
			// $bobot_butuh_berkurang = $bobot_butuh - $bobot_jual;

			if ($harga > $kopi_mentah->harga) {
				$error = "Harga melewati harga maximal (per kg)";
			}

			if ($bobot_bertambah > $bobot_butuh) {
				$error = "Maaf bobot Anda kelebihan dari bobot yang kami butuhkan";
			}

			if (!$error) {
				$data_kopimentah = array(
					"bobot"		=> 	$bobot_bertambah,
				);

				$isSuccess2 = $this->kopimentah_model->edit($id_kopi_mentah, $data_kopimentah);

				if ($isSuccess2) {
					$inserted_id_transaksi = $this->jualkopi_model->insertJualKopiAdmin($data_jual_kopi);

					// $data_pass["jenis_transaksi"] = "kopi_admin";
					// $id_kopi_mentah = $this->kopimentah_model->insert($data_pass);
					$image_location = "";

					if (isset($_FILES['gambar_kopi']) && is_uploaded_file($_FILES['gambar_kopi']['tmp_name'])) {
						# Upload gambar
						# -------------------------------------------------------------------
						$config['upload_path'] = "./uploads/";
						$config['file_name'] = "kopi_mentah_" . $inserted_id_transaksi;
						$config['allowed_types'] = 'gif|jpg|png';
						// $config['encrypt_name'] = TRUE;

						$this->load->library('upload', $config);

						if (!$this->upload->do_upload("gambar_kopi")) {
							$data['message'] = $this->upload->display_errors();
							$data['success'] = false;
							echo json_encode($data);
							return;
						}

						if ($inserted_id_transaksi) {
							// $data_image = array(
							// 	"image_location" => "uploads/{$config['file_name']}" . $this->upload->data('file_ext'),
							// );

							// $this->kopimentah_model->edit($id_kopi_mentah, $data_image);
							$image_location = "uploads/{$config['file_name']}" . $this->upload->data('file_ext');
						}
						# -------------------------------------------------------------------
					}

					$data_jual_kopi = array();
					$data_jual_kopi["image_location"]  = $image_location;

					if (isset($image_location)){
						$affected_rows = $this->jualkopi_model->edit($inserted_id_transaksi, $data_jual_kopi);
						$isSuccess = $affected_rows;
					}
				}
			}

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Jual kopi berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $error;
				$data['message'] = 'Jual kopi gagal!';

				if (!empty($error)) {
					$data["message"] = $error;
				}
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}
}
