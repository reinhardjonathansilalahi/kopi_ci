<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "controllers/petani/TransaksiJualKopiController.php";

class PenjualController extends CI_Controller
{
	USE TransaksiJualKopiController;

	function __construct()
	{
		parent::__construct();
		$this->load->model('MemberModel', "member_model");
		$this->load->model("KopiMentahModel", "kopimentah_model");
		$this->load->model("JualKopiModel", "jualkopi_model");
	}

	public function index()
	{
		$this->load->view('ViewHome');
	}

	public function login()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		$data_pass["table_name"] = "petani";

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$isSuccess = false;
			$query = $this->member_model->getMember($data_pass);

			// if there are no errors process our form, then return a message
			if ($query->num_rows() > 0) {
				$isSuccess = true;

				$petani = $query->row();
				$this->session->set_userdata("id_petani", $petani->id_petani);
				$this->session->set_userdata("role", $data_pass["role"]);
			}

			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Login success!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Login failed!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function register()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		$table_name = "petani";

		// Unset the non column key value data_pass for inserting new row item for database table
		unset($data_pass["role"]);

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			// the value inside isSucces is the insertedRow count
			$isSuccess = $this->member_model->insert($table_name, $data_pass);

			// DO ALL YOUR FORM PROCESSING HERE
			// THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Register success!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Register failed!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}
}
